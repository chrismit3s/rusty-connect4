from app import PxPoint, Img
from app.helpers import bgr_color, debug_show, apply_bbox, interval_mids, resetable_property, timed
from app import COLS, ROWS
from app.player import Player
from connect4 import Wrapper as Solver
from mss import mss
from pynput.mouse import Button, Controller as Mouse
from typing import Iterator
import cv2
import numpy as np


class GameHandle:
    BORDER_COLOR = bgr_color(173, 135, 58)
    BUTTON_COLOR = bgr_color(255, 138, 68)
    WHITE = bgr_color(255, 255, 255)
    BLACK = bgr_color(51, 51, 51)

    TEXT_CUTOFF = 128
    TEXT_RATIO = 7
    BUTTON_SIZE = 2000

    def __init__(self, depth) -> None:
        self.sct = mss()
        self.depth = depth
        self.solver = Solver(self.depth)
        self.mouse = Mouse()

    def __del__(self) -> None:
        self.sct.close()

    @property
    def board_img(self) -> Img:
        return apply_bbox(self.screen, self.board_bbox)

    @property
    def turn_img(self) -> Img:
        return apply_bbox(self.screen, self.turn_bbox)

    @resetable_property
    def buttons(self) -> list[PxPoint]:
        button_mask = (self.screen == GameHandle.BUTTON_COLOR).all(axis=2)
        if np.count_nonzero(button_mask) == 0:
            return []
        contours, _ = cv2.findContours(button_mask.astype(np.uint8), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        midpoints = []
        for contour in contours:
            x, y, w, h = cv2.boundingRect(contour)
            bbox = (y, x, y + h, x + w)
            color_pixels = np.count_nonzero(apply_bbox(button_mask, bbox))
            if color_pixels > GameHandle.BUTTON_SIZE:
                midpoints.append((x + w // 2, y + h // 2))
        return sorted(midpoints)

    @resetable_property
    def board(self) -> list[list[Player]]:
        ret = []
        row_px, col_px, *_ = self.board_img.shape
        for c in interval_mids(0, col_px, COLS):
            ret.append([])
            for r in interval_mids(row_px, 0, ROWS):
                color = self.board_img[r, c]
                if np.all(color == GameHandle.WHITE):
                    break
                ret[-1].insert(0, Player.from_color(color))
        return ret

    @resetable_property
    def current_piece(self) -> Player:
        _, w, *_ = self.turn_img.shape
        return Player.from_color(np.mean(self.turn_img[:, w // 2], axis=0))

    @resetable_property
    def is_player_turn(self) -> bool | None:
        text_mask = np.linalg.norm(self.turn_img - GameHandle.BLACK, axis=2) < GameHandle.TEXT_CUTOFF
        if np.count_nonzero(text_mask) == 0:
            return None
        tl = np.min(text_mask.nonzero(), axis=1)
        br = np.max(text_mask.nonzero(), axis=1)
        h, w = br - tl
        ratio = w / h
        return bool(ratio > GameHandle.TEXT_RATIO)

    def clear(self) -> None:
        self.solver = Solver(self.depth)

    def updates(self) -> Iterator[None]:
        while True:
            while not self.update():
                pass
            yield

    def update(self) -> bool:
        # get screen
        mon = self.sct.monitors[0]
        scr = np.asarray(self.sct.grab(mon))[:, :, :3]  # remove alpha
        self.screen = np.array(scr, dtype=np.uint8)

        # get board bbox
        border_mask = (self.screen == GameHandle.BORDER_COLOR).all(axis=2)
        if np.count_nonzero(border_mask) == 0:
            return False
        x0, y0 = np.min(border_mask.nonzero(), axis=1)
        x1, y1 = np.max(border_mask.nonzero(), axis=1)

        # get turn info bbox
        line_mask = (self.screen[:, y0:y1] == GameHandle.WHITE).all(axis=(1, 2))
        if np.count_nonzero(line_mask) == 0:
            return False
        x2 = np.max(line_mask.nonzero())

        self.board_bbox = (x0, y0, x1, y1)
        self.turn_bbox = (x1, y0, x2, y1)
        del self.board
        del self.buttons
        del self.current_piece
        del self.is_player_turn
        return True

    def is_gameover(self) -> bool:
        self.update()
        return self.solver.is_gameover()

    def best_move(self) -> int | None:
        if self.board is None or self.current_piece is None or self.is_player_turn is None or len(self.buttons) != 0:
            return None
        b = [[(piece == self.current_piece) == self.is_player_turn for piece in col] for col in self.board]
        self.solver.load_board(b)
        with timed("backtrack"):
            return self.solver.backtrack(self.is_player_turn)

    def make_move(self, move: int) -> None:
        x0, y0, x1, y1 = self.board_bbox
        r = list(interval_mids(x1, x0, ROWS))[-1]
        c = list(interval_mids(y0, y1, COLS))[move]
        self.click((c, r))

    def click_button(self, n: int) -> None:
        self.click(self.buttons[n])

    def click(self, pos: PxPoint) -> None:
        old_position = self.mouse.position

        self.mouse.position = pos
        self.mouse.click(Button.left)
        print("clicked", pos)

        self.mouse.position = old_position

    def debug(self, do_print=False) -> None:
        if do_print:
            print("Board:")
            if self.board is None:
                print("no board found")
            else:
                print("\n".join("".join(repr(x) for x in c) for c in self.board))
            print("Current player:", self.current_piece)
            print("Our turn:", self.is_player_turn)
            print("Buttons:", self.buttons)
            print("-" * 20)

        debug_show("board", self.board_img)
        debug_show("turn", self.turn_img)
        return cv2.waitKey(1000 // 60)


if __name__ == "__main__":
    col_chars = [ord(str(c)) for c in range(1, COLS + 1)]
    button_chars = [ord(c) for c in "abcdefghijklmnoprstuvwxyz"]  # no q
    h = GameHandle(8)
    try:
        do_print = False
        quit = False
        for _ in h.updates():
            ret = h.debug(do_print)
            if ret in col_chars:
                h.make_move(col_chars.index(ret))
            if ret in button_chars:
                h.click(h.buttons[button_chars.index(ret)])
            do_print = ret == ord(" ")
            quit = ret == ord("q")
    except KeyboardInterrupt:
        pass
