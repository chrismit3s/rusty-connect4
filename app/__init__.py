from numpy.typing import NDArray
import numpy as np
import cv2

BGRColor = NDArray[np.uint8]
BBox = tuple[int, int, int, int]
Img = NDArray[np.uint8]
PxPoint = tuple[int, int]

SIZE = (COLS, ROWS) = (7, 6)
DEBUG_IMG: Img = cv2.imread("debug.png")
DEBUG_BLEND_FACTOR = 0.1
