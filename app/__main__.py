from app.handle import GameHandle
from app.helpers import debug_show, apply_bbox
from time import sleep
import cv2
import numpy as np

h = GameHandle(9)
print("created handle")

for _ in h.updates():
    if len(h.buttons) != 0:
        pos = h.buttons[0]

        img = np.array(h.screen)
        for z in h.buttons:
            y, x = z
            img[(x - 10) : (x + 10), (y - 10) : (y + 10), :] = 0
        debug_show("buttons", apply_bbox(img, h.board_bbox))
        cv2.waitKey(10000)

        h.click(pos)
        print("clicked button at", pos)
        sleep(0.1)
    if not h.is_player_turn:
        move = h.best_move()
        if move is None:
            print("move was none")
            continue
        h.make_move(move)
        print("made move", move)
        sleep(0.1)
