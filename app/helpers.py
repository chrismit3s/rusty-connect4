from app import BBox, Img, BGRColor
from app import DEBUG_IMG, DEBUG_BLEND_FACTOR
from typing import Iterator
from functools import wraps
from contextlib import contextmanager
from time import time
import cv2
import numpy as np


def bgr_color(b, g, r) -> BGRColor:
    return np.array((b, g, r), dtype=np.uint8)


def debug_show(title: str, img: Img) -> None:
    # add a shadow to the preview
    shadow = cv2.resize(DEBUG_IMG, img.shape[1::-1])
    img = cv2.addWeighted(img, 1 - DEBUG_BLEND_FACTOR, shadow, DEBUG_BLEND_FACTOR, 0)
    cv2.imshow(title, img)


def apply_bbox(img: Img, bbox: BBox):
    x0, y0, x1, y1 = bbox
    return img[x0:x1, y0:y1]


def interval_mids(lower: int, upper: int, n: int) -> Iterator[int]:
    step = (upper - lower) / n
    for i in range(n):
        yield lower + round(step * (i + 0.5))


@contextmanager
def timed(prompt: str):
    print(prompt, end="", flush=True)
    started = time()
    yield
    elapsed = time() - started
    print(f" - took {elapsed:.2f} seconds")


def resetable_property(f):
    return ResetableProperty(f)


class ResetableProperty:
    def __init__(self, f):
        self.attr = "_" + f.__name__
        self.compute = f

    def __get__(self, obj, owner=None):
        value = getattr(obj, self.attr, None)
        if value is None:
            value = self.compute(obj)
            setattr(obj, self.attr, value)
        return value

    def __delete__(self, obj):
        setattr(obj, self.attr, None)
