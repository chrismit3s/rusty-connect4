from __future__ import annotations
from enum import IntEnum
from app import BGRColor
import numpy as np

_COLORS: list[BGRColor] = [
    np.array((0xFF, 0, 0), dtype=np.uint8),
    np.array((0, 0, 0xFF), dtype=np.uint8),
]

_NAMES: list[str] = [
    "BLUE",
    "RED",
]


class Player(IntEnum):
    BLUE = 0
    RED = 1

    @staticmethod
    def from_color(color: BGRColor) -> Player:
        return min(Player, key=lambda p: np.linalg.norm(color - p.color()))  # type: ignore

    def __repr__(self):
        return _NAMES[self][0]

    def __str__(self):
        return _NAMES[self]

    def color(self) -> BGRColor:
        return _COLORS[self]

    def other(self) -> Player:
        return Player(1 - self)
