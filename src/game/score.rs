use std::cmp::Ordering;
use std::fmt::{self, Display, Formatter};
use std::ops::{Add, AddAssign, Sub, SubAssign};

use crate::bitrepr::{BitRepr, FromBitRepr};
use crate::game::Outcome;
use crate::game::Piece;

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Debug)]
pub struct Score(i8);

impl Score {
    pub const MAX: Self = Self(Self::MAX_VALUE);
    pub const ZERO: Self = Self(0);
    pub const MIN: Self = Self(Self::MIN_VALUE);

    pub const MAX_VALUE: i8 = i8::MAX;
    pub const MIN_VALUE: i8 = -i8::MAX;

    pub fn from(s: i8) -> Self {
        Self(Self::clamp(s))
    }

    pub fn value(&self) -> i8 {
        self.0
    }

    pub fn abs(&self) -> u8 {
        self.0.unsigned_abs()
    }

    pub fn expected_outcome(&self) -> Outcome {
        match self.cmp(&Self::ZERO) {
            Ordering::Greater => Outcome::winner(Piece::Player),
            Ordering::Equal => Outcome::draw(),
            Ordering::Less => Outcome::winner(Piece::Computer),
        }
    }

    fn clamp(s: i8) -> i8 {
        s.clamp(Self::MIN_VALUE, Self::MAX_VALUE)
    }
}

impl BitRepr for Score {
    type Repr = u8;

    const SIZE: usize = Self::Repr::BITS as usize;

    fn bit_repr(&self) -> Self::Repr {
        self.0 as Self::Repr
    }
}

impl FromBitRepr for Score {
    fn from_bit_repr(repr: Self::Repr) -> Self {
        Self::from(repr as i8)
    }
}

impl Add<Self> for Score {
    type Output = Self;

    fn add(self, other: Self) -> Self::Output {
        Self::from(self.0.saturating_add(other.0))
    }
}

impl AddAssign<Self> for Score {
    fn add_assign(&mut self, other: Self) {
        self.0 = Self::clamp(self.0.saturating_add(other.0));
    }
}

impl Sub<Self> for Score {
    type Output = Self;

    fn sub(self, other: Self) -> Self::Output {
        Self::from(self.0.saturating_sub(other.0))
    }
}

impl SubAssign<Self> for Score {
    fn sub_assign(&mut self, other: Self) {
        self.0 = Self::clamp(self.0.saturating_sub(other.0));
    }
}

impl Display for Score {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self.expected_outcome() {
            Outcome::Winner(piece) =>
                f.pad_integral(true, &piece.to_string(), &self.abs().to_string()),
            Outcome::Draw => f.pad_integral(true, "", "0"),
        }
    }
}
