use colored::*;

use rand::seq::IteratorRandom;
use rand::Rng;

use std::fmt::{self, Display};
use std::ops::{Index, IndexMut};

use crate::bitrepr::{self, BitRepr};
use crate::game::Col;
use crate::game::Connect;
use crate::game::Mask;
use crate::game::Outcome;
use crate::game::Piece;
use crate::game::Score;

#[derive(Clone, PartialEq, Eq, Debug, Hash)]
pub struct Board {
    cols: Box<[Col]>,
}

impl Board {
    pub const DEFAULT_COLS: usize = 7;
    pub const TO_WIN: usize = 4;
    pub const WIN_MASK: Mask = !(!0 << Board::TO_WIN);

    const WIDE_TABLE: bool = false;

    pub fn new(num_cols: usize) -> Self {
        debug_assert!(
            num_cols * <Col as BitRepr>::SIZE <= <Self as BitRepr>::Repr::BITS as usize,
            "Board is too big for bitrepr"
        );
        Self::from(vec![Col::new(); num_cols])
    }

    pub fn from(cols: Vec<Col>) -> Self {
        Self {
            cols: cols.into_boxed_slice(),
        }
    }

    pub fn random<R: ?Sized + Rng>(cols: usize, rng: &mut R) -> Self {
        let mut board = Board::new(cols);
        for _ in 0..(Board::TO_WIN - 1) {
            for &turn in Piece::ALL {
                let c = board.iter_possible_moves().choose(rng).unwrap();
                board[c].push(turn).unwrap();
            }
        }
        board
    }

    pub fn is_empty(&self) -> bool {
        self.cols.is_empty()
    }

    pub fn len(&self) -> usize {
        self.cols.len()
    }

    pub fn eval_with<T, F>(&mut self, c: usize, piece: Piece, f: F) -> T
    where
        F: FnOnce(&Self) -> T,
    {
        self[c].push(piece).unwrap();
        let t = f(self);
        self[c].pop().unwrap();
        t
    }

    pub fn move_score(&mut self, c: usize, piece: Piece) -> Score {
        let other = piece.other();
        let mut score = 0.0;
        for connect in self.iter_possible_connects(c) {
            if let Some(missing) = self.missing_for_connect(connect, piece) {
                score += piece.sign() as f32 / missing.saturating_sub(1) as f32;
            }

            if let Some(missing) = self.missing_for_connect(connect, other) {
                score += other.sign() as f32 / missing as f32;
            }
        }
        Score::from(score as i8)
    }

    pub fn score(&self) -> Score {
        // TODO caching in 4 col blocks?
        let mut score = 0.0;
        for connect in self.iter_all_connects() {
            for piece in Piece::ALL.iter().copied() {
                if let Some(missing) = self.missing_for_connect(connect, piece) {
                    score += piece.sign() as f32 / missing as f32;
                }
            }
        }
        Score::from(score as i8)
    }

    pub fn eval_with_mut<T, F>(&mut self, c: usize, piece: Piece, f: F) -> T
    where
        F: FnOnce(&mut Self) -> T,
    {
        self[c].push(piece).unwrap();
        let t = f(self);
        self[c].pop().unwrap();
        t
    }

    pub fn get(&self, c: usize, r: usize) -> Option<Piece> {
        self.cols.get(c).and_then(|col| col.get(r))
    }

    pub fn outcome(&self) -> Option<Outcome> {
        if self.cols.iter().all(Col::is_full) {
            return Some(Outcome::draw());
        }

        for &p in Piece::ALL {
            let masks: Vec<_> = self.cols.iter().map(|col| col.mask(&p)).collect();
            let outcome = Outcome::from_masks(p, &masks);
            if !outcome.is_draw() {
                return Some(outcome);
            }
        }
        None
    }

    pub fn outcome_range(&self) -> [Outcome; Piece::NUM_VARIANTS] {
        Piece::for_each(|p| {
            let masks: Box<[_]> = self
                .cols
                .iter()
                .map(|col| !col.mask(&p.other()) & col.full_mask())
                .collect();
            Outcome::from_masks(p, &masks)
        })
    }

    pub fn is_draw(&self) -> bool {
        self.outcome()
            .as_ref()
            .map(Outcome::is_draw)
            .unwrap_or(false)
    }

    pub fn has_winner(&self) -> bool {
        self.outcome()
            .as_ref()
            .map(Outcome::has_winner)
            .unwrap_or(false)
    }

    pub fn iter_possible_moves(&self) -> impl Iterator<Item = usize> + '_ {
        (0..self.cols.len()).filter(|&c| !self[c].is_full())
    }

    pub fn iter_all_connects(&self) -> impl Iterator<Item = Connect> + '_ {
        Connect::iter_all(self.cols.len(), Col::NUM_ROWS)
    }

    pub fn iter_possible_connects(&self, c: usize) -> impl Iterator<Item = Connect> + '_ {
        Connect::iter_for(c, self[c].next_row(), self.cols.len(), Col::NUM_ROWS)
    }

    pub fn mask_for_connect(&self, pos: Connect, piece: Piece) -> Mask {
        pos.iter_indices()
            .map(|(c, r)| (self[c][r] == Some(piece)) as Mask)
            .fold(0, |m, x| (m << 1) | x)
    }

    pub fn missing_for_connect(&self, pos: Connect, piece: Piece) -> Option<u32> {
        let available = !self.mask_for_connect(pos, piece.other()) & Board::WIN_MASK;
        let num_missing = Board::TO_WIN as u32 - self.mask_for_connect(pos, piece).count_ones();

        // not all required cells are available
        if available != Board::WIN_MASK {
            return None;
        }

        match pos {
            Connect::MultiCol { .. } => Some(num_missing),
            Connect::SingleCol { .. } if num_missing <= 1 => Some(num_missing),
            Connect::SingleCol { .. } => None,
        }
    }
}

impl BitRepr for Board {
    type Repr = u64;

    const SIZE: usize = Self::Repr::BITS as usize;

    fn bit_repr(&self) -> Self::Repr {
        self.cols.iter().copied().fold(0, bitrepr::combine)
    }
}

impl Index<usize> for Board {
    type Output = Col;

    fn index(&self, index: usize) -> &Self::Output {
        &self.cols[index]
    }
}

impl IndexMut<usize> for Board {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        &mut self.cols[index]
    }
}

impl Display for Board {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // vertical line is 0x2503, horizontal line is 0x2501, cross is 0x254B
        // | 0x2503
        // - 0x2501
        // + 0x254B
        const COL_SEP: &str = if Board::WIDE_TABLE {
            " \u{2503} "
        } else {
            "\u{2503}"
        };
        const CROSS_SEP: &str = if Board::WIDE_TABLE {
            "\u{2501}\u{254B}\u{2501}"
        } else {
            "\u{254B}"
        };
        const ROW_SEP: &str = "\u{2501}";

        for r in (0..Col::NUM_ROWS).rev() {
            writeln!(
                f,
                "{}",
                (0..self.cols.len())
                    .map(|c| match self[c][r] {
                        Some(Piece::Computer) => "C".black().on_red(),
                        Some(Piece::Player) => "P".black().on_yellow(),
                        None => " ".normal(),
                    }
                    .to_string())
                    .collect::<Vec<_>>()
                    .join(COL_SEP)
            )?;
        }
        writeln!(
            f,
            "{}",
            (0..self.cols.len())
                .map(|_| ROW_SEP)
                .collect::<Vec<_>>()
                .join(CROSS_SEP)
        )?;
        write!(
            f,
            "{}",
            (0..self.cols.len())
                .map(|c| c.to_string())
                .collect::<Vec<_>>()
                .join(COL_SEP)
        )?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use test::Bencher;

    use rand_pcg::Pcg64Mcg;

    use crate::SEED;

    fn create_board(cols: usize, b: &[&str]) -> Board {
        let mut board = Board::new(cols);
        b.iter()
            .rev()
            .flat_map(|r| r.chars().enumerate())
            .for_each(|(i, c)| match c.to_ascii_lowercase() {
                'y' => board[i].push(Piece::Player).unwrap(),
                'r' => board[i].push(Piece::Computer).unwrap(),
                _ => (),
            });
        board
    }

    #[test]
    fn test_possible_connects() {
        const SQUARE_SIZE: usize = 7;
        let board = create_board(SQUARE_SIZE, &["   r   ", "   y   ", "   r   "]);
        let num_connects = board.iter_possible_connects(SQUARE_SIZE / 2).count();
        assert_eq!(num_connects, Board::TO_WIN + 3 * (Board::TO_WIN - 1));

        let num_connects = board.iter_possible_connects(0).count();
        assert_eq!(num_connects, 3);

        let num_connects = board.iter_possible_connects(SQUARE_SIZE - 1).count();
        assert_eq!(num_connects, 3);
    }

    #[test]
    fn test_outcome() {
        // col
        let board = create_board(Board::DEFAULT_COLS, &[
            "y   rr ", "yr yyr ", "yryrryy", "yryrryr",
        ]);
        assert_eq!(
            board.outcome(),
            Some(Outcome::winner(Piece::Player)),
            "\n{}\n",
            board
        );

        // row
        let board = create_board(Board::DEFAULT_COLS, &["r   rr ", "yrrrryy", "yryrryr"]);
        assert_eq!(
            board.outcome(),
            Some(Outcome::winner(Piece::Computer)),
            "\n{}\n",
            board
        );

        // diagonal ascending
        let board = create_board(Board::DEFAULT_COLS, &[
            "y   yr ", "rr yyr ", "rryrryy", "ryyrryr",
        ]);
        assert_eq!(
            board.outcome(),
            Some(Outcome::winner(Piece::Player)),
            "\n{}\n",
            board
        );

        // diagonal descending
        let board = create_board(Board::DEFAULT_COLS, &[
            "r   yr ", "rr yyr ", "ryryryy", "ryyrryr",
        ]);
        assert_eq!(
            board.outcome(),
            Some(Outcome::winner(Piece::Computer)),
            "\n{}\n",
            board
        );
    }

    fn bench_score_ncols(n: usize, b: &mut Bencher) {
        let mut rng = Pcg64Mcg::new(SEED);
        b.iter(|| Board::random(n, &mut rng).score());
    }

    fn bench_move_score_ncols(n: usize, b: &mut Bencher) {
        let mut rng = Pcg64Mcg::new(SEED);
        b.iter(|| {
            let col = rng.gen_range(0..n);
            let piece = if rng.gen() {
                Piece::Computer
            } else {
                Piece::Player
            };
            Board::random(n, &mut rng).move_score(col, piece);
        });
    }

    #[bench]
    fn bench_score_3cols(b: &mut Bencher) {
        bench_score_ncols(3, b);
    }

    #[bench]
    fn bench_score_4cols(b: &mut Bencher) {
        bench_score_ncols(4, b);
    }

    #[bench]
    fn bench_score_5cols(b: &mut Bencher) {
        bench_score_ncols(5, b);
    }

    #[bench]
    fn bench_move_score_3cols(b: &mut Bencher) {
        bench_move_score_ncols(3, b);
    }

    #[bench]
    fn bench_move_score_4cols(b: &mut Bencher) {
        bench_move_score_ncols(4, b);
    }

    #[bench]
    fn bench_move_score_5cols(b: &mut Bencher) {
        bench_move_score_ncols(5, b);
    }
}
