use std::fmt::{self, Debug, Display, Formatter};
use std::ops::Index;

use crate::bitrepr::BitRepr;
use crate::game::Piece;

pub type Mask = u8;

#[derive(Copy, Clone, PartialEq, Eq, Hash)]
pub struct Col {
    mask: Mask,
    next_row: usize,
}

impl Col {
    pub const NUM_ROWS: usize = 6;

    const VALUES: &[Option<Piece>] = &[Some(Piece::ALL[0]), Some(Piece::ALL[1])];

    pub fn new() -> Self {
        Col {
            next_row: 0,
            mask: 0,
        }
    }

    pub fn next_row(&self) -> usize {
        self.next_row
    }

    pub fn is_empty(&self) -> bool {
        self.next_row == 0
    }

    pub fn is_full(&self) -> bool {
        self.next_row == Self::NUM_ROWS
    }

    pub fn push(&mut self, p: Piece) -> Result<(), String> {
        if self.is_full() {
            return Err(String::from("Column is full"));
        }

        self.mask |= (p.index() as u8) << self.next_row;
        self.next_row += 1;
        Ok(())
    }

    pub fn pop(&mut self) -> Result<Piece, String> {
        if self.is_empty() {
            return Err(String::from("Column is emtpy"));
        }

        let ret = self.get(self.next_row - 1).unwrap();
        self.next_row -= 1;
        self.mask &= self.curr_mask();
        Ok(ret)
    }

    pub fn curr_mask(&self) -> Mask {
        !(!0 << self.next_row)
    }

    pub fn full_mask(&self) -> Mask {
        !(!0 << Self::NUM_ROWS)
    }

    pub fn mask(&self, p: &Piece) -> Mask {
        match p {
            Piece::Computer => !self.mask & self.curr_mask(),
            Piece::Player => self.mask,
        }
    }

    fn bit_at(&self, r: usize) -> Mask {
        (self.mask >> r) & 1
    }

    pub fn get(&self, r: usize) -> Option<Piece> {
        (r < self.next_row).then(|| Piece::ALL[self.bit_at(r) as usize])
    }
}

impl Default for Col {
    fn default() -> Self {
        Self::new()
    }
}

impl BitRepr for Col {
    type Repr = u8;

    const SIZE: usize = Col::NUM_ROWS + 1;

    fn bit_repr(&self) -> Self::Repr {
        self.mask | (1 << self.next_row)
    }
}

impl Index<usize> for Col {
    type Output = Option<Piece>;

    fn index(&self, r: usize) -> &'static Self::Output {
        if r < Self::NUM_ROWS {
            if r < self.next_row {
                &Self::VALUES[self.bit_at(r) as usize]
            } else {
                &None
            }
        } else {
            panic!("Index {} is out of bounds for col {:?}", r, self);
        }
    }
}

impl Debug for Col {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        let mask = format!("{:0width$b}", self.mask, width = Self::NUM_ROWS);
        f.write_str("|")?;
        f.pad_integral(true, "mask=", &mask)?;
        f.write_str(if f.alternate() { ", " } else { " " })?;
        f.pad_integral(true, "next=", &self.next_row.to_string())?;
        f.write_str("|")?;
        Ok(())
    }
}

impl Display for Col {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        let s = (0..Self::NUM_ROWS)
            .rev()
            .map(|i| match self.get(i) {
                None => ' ',
                Some(p) => p.as_str().chars().next().unwrap(),
            })
            .fold(String::with_capacity(Self::NUM_ROWS), |mut s, c| {
                s.push(c);
                s
            });

        f.write_str("|")?;
        f.write_str(&s)?;
        f.write_str("|")?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use std::collections::HashSet;

    fn build_col_from_mask(mask: Mask, rows: usize) -> Col {
        let mut col = Col::new();
        for i in 0..rows {
            let index = (mask >> i) & 1;
            col.push(Piece::ALL[index as usize]).unwrap();
        }
        col
    }

    #[test]
    fn test_bit_repr_unique() {
        let mut set = HashSet::new();
        let mut i = 0;
        for rows in 0..Col::NUM_ROWS {
            for mask in 0..(1 << rows) {
                let col = build_col_from_mask(mask, rows);
                set.insert(col.bit_repr());
                i += 1;
            }
        }
        assert_eq!(set.len(), i);
    }

    #[test]
    fn test_col_size() {
        assert!(
            Col::NUM_ROWS <= Mask::BITS as usize,
            "Col is too many rows for mask"
        );
        assert!(
            Col::NUM_ROWS <= <Col as BitRepr>::Repr::BITS as usize,
            "Col is too many rows for bitrepr"
        );
    }
}
