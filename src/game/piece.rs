use std::cmp::Ordering;
use std::fmt::{self, Display, Formatter};

use crate::game::Score;

#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub enum Piece {
    Computer, // minimizing
    Player,   // maximizing
}

impl Piece {
    pub const NUM_VARIANTS: usize = 2;
    pub const ALL: &[Self] = &[Piece::Computer, Piece::Player];

    const NAMES: &[&'static str] = &["Computer", "Player"];

    pub fn for_each<T, F>(f: F) -> [T; Self::NUM_VARIANTS]
    where
        F: Fn(Piece) -> T,
    {
        [f(Piece::Computer), f(Piece::Player)]
    }

    pub fn as_str(&self) -> &'static str {
        Self::NAMES[self.index()]
    }

    pub fn is_maximizing(&self) -> bool {
        *self == Piece::Player
    }

    pub fn other(&self) -> Self {
        match self {
            Piece::Computer => Piece::Player,
            Piece::Player => Piece::Computer,
        }
    }

    pub fn index(&self) -> usize {
        match self {
            Piece::Computer => 0,
            Piece::Player => 1,
        }
    }

    pub fn sign(&self) -> i8 {
        match self {
            Piece::Computer => -1,
            Piece::Player => 1,
        }
    }

    pub fn score(&self) -> Score {
        match self {
            Piece::Computer => Score::MIN,
            Piece::Player => Score::MAX,
        }
    }

    pub fn ord(&self, ord: Ordering) -> Ordering {
        match self {
            Piece::Computer => ord.reverse(),
            Piece::Player => ord,
        }
    }

    pub fn cmp<O: Ord>(&self, x: &O, y: &O) -> Ordering {
        self.ord(x.cmp(y))
    }
}

impl Display for Piece {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        f.pad(self.as_str())
    }
}
