use std::ops::{Shl, Shr};

#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub enum Direction {
    Horizontal,
    Descending,
    Ascending,
}

impl Direction {
    pub const ALL: &[Self] = &[Self::Descending, Self::Horizontal, Self::Ascending];

    pub fn from_sign(sign: i8) -> Result<Self, String> {
        match sign {
            -1 => Ok(Direction::Descending),
            0 => Ok(Direction::Horizontal),
            1 => Ok(Direction::Ascending),
            d => Err(format!("Invalid sign: {}", d)),
        }
    }

    pub fn shift<T, S>(&self, t: T, shift: S) -> T
    where
        T: Shl<S, Output = T> + Shr<S, Output = T>,
    {
        match self {
            Direction::Descending => t >> shift,
            Direction::Horizontal => t,
            Direction::Ascending => t << shift,
        }
    }

    pub fn sign(&self) -> i8 {
        match self {
            Direction::Descending => -1,
            Direction::Horizontal => 0,
            Direction::Ascending => 1,
        }
    }

    pub fn iter() -> impl Iterator<Item = Self> {
        Self::ALL.iter().copied()
    }
}
