use std::iter;

use crate::game::Board;
use crate::game::Direction;
use crate::helper::{cartesian_product, offset};

#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub enum Connect {
    SingleCol {
        col: usize,
        row: usize,
    },
    MultiCol {
        col: usize,
        row: usize,
        dir: Direction,
    },
}

impl Connect {
    pub fn stack(col: usize, row: usize) -> Self {
        Self::SingleCol { col, row }
    }

    pub fn line(col: usize, row: usize, dir: Direction) -> Self {
        Self::MultiCol { col, row, dir }
    }

    pub fn line_from_sign(col: usize, row: usize, sign: i8) -> Result<Self, String> {
        Direction::from_sign(sign).map(|dir| Self::MultiCol { col, row, dir })
    }

    pub fn step(&self) -> (i8, i8) {
        match *self {
            Self::SingleCol { .. } => (0, 1),
            Self::MultiCol { dir, .. } => (1, dir.sign()),
        }
    }

    pub fn start(&self) -> (usize, usize) {
        match *self {
            Self::SingleCol { col, row } => (col, row),
            Self::MultiCol { col, row, .. } => (col, row),
        }
    }

    pub fn iter_indices(&self) -> impl Iterator<Item = (usize, usize)> {
        let (cx, rx) = self.step();
        let (c, r) = self.start();

        iter::successors(Some((c, r)), move |&(ci, ri)| {
            Some((offset(ci, cx as isize), offset(ri, rx as isize)))
        })
        .take(Board::TO_WIN)
    }

    pub fn iter_for(
        c: usize,
        r: usize,
        num_cols: usize,
        num_rows: usize,
    ) -> impl Iterator<Item = Self> {
        const RADIUS: isize = Board::TO_WIN as isize - 1;
        let cols_left = RADIUS.min(c as isize);
        let cols_right = RADIUS.min((num_cols - c - 1) as isize); // TODO -1

        let rows_below = RADIUS.min(r as isize).max(0);
        let rows_above = RADIUS.min((num_rows - r - 1) as isize).max(0);

        let stack_wins =
            (-rows_below..=(rows_above - RADIUS)).map(move |x| Self::stack(c, offset(r, x)));

        let hor_wins = (-cols_left..=(cols_right - RADIUS))
            .map(move |x| Self::line(offset(c, x), r, Direction::Horizontal));

        let asc_down = cols_left.min(rows_below);
        let asc_up = cols_right.min(rows_above);
        let asc_wins = (-asc_down..=(asc_up - RADIUS))
            .map(move |x| Self::line(offset(c, x), offset(r, x), Direction::Ascending));

        let dsc_down = cols_left.min(rows_above);
        let dsc_up = cols_right.min(rows_below);
        let dsc_wins = (-dsc_down..=(dsc_up - RADIUS))
            .map(move |x| Self::line(offset(c, x), offset(r, -x), Direction::Descending));

        stack_wins.chain(hor_wins).chain(asc_wins).chain(dsc_wins)
    }

    pub fn iter_all(num_cols: usize, num_rows: usize) -> impl Iterator<Item = Self> {
        let all_cols = 0..num_cols;
        let line_cols = 0..(num_cols.saturating_sub(Board::TO_WIN));

        let all_rows = 0..num_rows;
        let bottom_rows = 0..(num_rows.saturating_sub(Board::TO_WIN));
        let top_rows = (Board::TO_WIN - 1)..num_rows;

        let stack_wins = cartesian_product(all_cols, bottom_rows.clone())
            .map(|(c, r)| Self::stack(c, r));

        let hor_wins = cartesian_product(line_cols.clone(), all_rows)
            .map(|(c, r)| Self::line(c, r, Direction::Horizontal));

        let asc_wins = cartesian_product(line_cols.clone(), bottom_rows)
            .map(|(c, r)| Self::line(c, r, Direction::Ascending));

        let dsc_wins = cartesian_product(line_cols, top_rows)
            .map(|(c, r)| Self::line(c, r, Direction::Descending));

        stack_wins.chain(hor_wins).chain(asc_wins).chain(dsc_wins)
    }
}
