use std::cmp::{Ordering, PartialOrd};
use std::fmt::{self, Display};
use std::iter;
use std::ops::BitAnd;

use crate::bitrepr::{BitRepr, FromBitRepr};
use crate::game::Board;
use crate::game::Direction;
use crate::game::Mask;
use crate::game::Piece;
use crate::game::Score;

fn stack_win(mask: Mask) -> bool {
    iter::successors(Some(mask), |&m| Some(m >> 1).filter(|&n| n != 0))
        .any(|m| (m & Board::WIN_MASK) == Board::WIN_MASK)
}

fn line_win(masks: &[Mask; Board::TO_WIN], dir: Direction) -> bool {
    masks
        .iter()
        .copied()
        .enumerate()
        .map(|(i, m)| dir.shift(m, i))
        .reduce(BitAnd::bitand)
        .unwrap()
        != 0
}

#[derive(Copy, Clone, PartialEq, Eq, Hash, Debug)]
pub enum Outcome {
    Winner(Piece),
    Draw,
}

impl Outcome {
    pub const LOWEST: Self = Self::Winner(Piece::Computer);
    pub const HIGHEST: Self = Self::Winner(Piece::Player);
    pub const ALL: &[Self] = &[Self::LOWEST, Self::Draw, Self::HIGHEST];

    pub fn from_masks(piece: Piece, masks: &[Mask]) -> Self {
        let has_won = masks.iter().copied().any(stack_win)
            || masks
                .windows(Board::TO_WIN)
                .map(|mask_slice| mask_slice.try_into().unwrap())
                .any(|mask_slice| Direction::iter().any(|dir| line_win(mask_slice, dir)));
        if has_won {
            Self::Winner(piece)
        } else {
            Self::Draw
        }
    }

    pub fn draw() -> Self {
        Self::Draw
    }

    pub fn loser(piece: Piece) -> Self {
        Self::Winner(piece.other())
    }

    pub fn winner(piece: Piece) -> Self {
        Self::Winner(piece)
    }

    pub fn score(&self) -> Score {
        match self {
            Self::Draw => Score::ZERO,
            Self::Winner(winner) => winner.score(),
        }
    }

    pub fn option(&self) -> Option<Piece> {
        match self {
            Self::Draw => None,
            Self::Winner(winner) => Some(*winner),
        }
    }

    pub fn is_draw(&self) -> bool {
        *self == Self::Draw
    }

    pub fn has_winner(&self) -> bool {
        !self.is_draw()
    }

    pub fn to_str(&self, view: Piece) -> &'static str {
        match self {
            Self::Draw => "draw",
            Self::Winner(winner) if *winner == view => "winning",
            Self::Winner(_) => "losing",
        }
    }
}

impl BitRepr for Outcome {
    type Repr = u8;

    const SIZE: usize = 2;

    fn bit_repr(&self) -> Self::Repr {
        match self {
            Self::Winner(Piece::Computer) => 0b01,
            Self::Winner(Piece::Player) => 0b10,
            Self::Draw => 0b00,
        }
    }
}

impl FromBitRepr for Outcome {
    fn from_bit_repr(repr: Self::Repr) -> Self {
        match repr {
            0b01 => Self::Winner(Piece::Computer),
            0b10 => Self::Winner(Piece::Player),
            0b00 => Self::Draw,
            x => panic!("Invalid bitrepr {:b}", x),
        }
    }
}

impl Display for Outcome {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::Winner(p) => writeln!(f, "{} won", p),
            Self::Draw => writeln!(f, "Draw"),
        }
    }
}

impl PartialOrd for Outcome {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Outcome {
    fn cmp(&self, other: &Self) -> Ordering {
        self.score().cmp(&other.score())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_bit_repr_fixpoint() {
        for oc in Outcome::ALL.iter() {
            assert_eq!(*oc, Outcome::from_bit_repr(oc.bit_repr()));
        }
    }
}
