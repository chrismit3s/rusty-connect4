use crate::backtracker::Backtracker;
use crate::game::Board;
use crate::game::Col;
use crate::game::Piece;
use pyo3::prelude::*;

fn read_piece(is_player: bool) -> Piece {
    if is_player {
        Piece::Player
    } else {
        Piece::Computer
    }
}

fn read_col(pieces: Vec<bool>) -> Col {
    let mut col = Col::new();
    pieces
        .into_iter()
        .for_each(|is_player| col.push(read_piece(is_player)).unwrap());
    col
}

fn read_board(pieces: Vec<Vec<bool>>) -> Board {
    Board::from(pieces.into_iter().map(read_col).collect())
}

#[pyclass]
pub struct Wrapper {
    bt: Backtracker,
    board: Board,
}

#[pymethods]
impl Wrapper {
    #[new]
    pub fn new(depth: Option<u8>) -> Self {
        Self {
            bt: Backtracker::new(depth),
            board: Board::new(Board::DEFAULT_COLS),
        }
    }

    pub fn make_move(&mut self, c: usize, is_player: bool) {
        self.board[c].push(read_piece(is_player)).unwrap()
    }

    pub fn load_board(&mut self, board: Vec<Vec<bool>>) {
        self.board = read_board(board);
    }

    pub fn is_gameover(&self) -> bool {
        self.board.outcome().is_some()
    }

    pub fn backtrack(&mut self, is_player: bool) -> Option<usize> {
        self.bt
            .backtrack_for(
                &mut self.board,
                if is_player {
                    Piece::Player
                } else {
                    Piece::Computer
                },
            )
            .col()
    }
}
