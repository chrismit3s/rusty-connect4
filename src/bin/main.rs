#![allow(unused_imports)]
use std::error::Error;
use std::io::{self, Write};
use std::time::Instant;

use clap::clap_app;

use connect4::backtracker::BacktrackReturn;
use connect4::backtracker::Backtracker;
use connect4::game::Board;
use connect4::game::Outcome;
use connect4::game::Piece;
use connect4::helper::format_duration;

fn main() -> Result<(), Box<dyn Error>> {
    let matches = clap_app!(myapp =>
        (author: "Christoph Schmidtmeier <chrissidtmeier@gmail.com>")
        (about: "Play Connect 4")
        (@arg cols: -c --cols +takes_value "The number of columns in the board")
        (@arg depth: -d --depth +takes_value "The search depth")
        (@group start =>
            (@arg player: -P "Player starts")
            (@arg computer: -C "Computer starts"))
    )
    .get_matches();

    let depth = matches.value_of("depth").map(str::parse).transpose()?;
    let cols = matches
        .value_of("cols")
        .map(str::parse)
        .transpose()?
        .unwrap_or(Board::DEFAULT_COLS);

    let mut bt = Backtracker::new(depth);
    let mut board = Board::new(cols);
    let mut turn = match matches.value_of("start") {
        Some("player") | None => Piece::Player,
        Some("computer") => Piece::Computer,
        Some(s) => panic!("Unknown argument '{}'", s),
    };

    while board.outcome().is_none() {
        println!("{}", board);
        let col = match turn {
            Piece::Player => player_move(&mut board, &mut bt),
            Piece::Computer => computer_move(&mut board, &mut bt),
        };
        board[col].push(turn).unwrap();
        turn = turn.other();
    }

    println!("{}", board);
    println!("{}", board.outcome().unwrap());

    Ok(())
}

fn player_move(board: &mut Board, bt: &mut Backtracker) -> usize {
    let mut buffer = String::new();
    loop {
        print!("Column? ");
        io::stdout().flush().expect("flush failed");

        buffer.clear();
        io::stdin()
            .read_line(&mut buffer)
            .expect("read_line failed");

        let trimmed = buffer.trim();
        if trimmed == "b" {
            return bt.backtrack_for(board, Piece::Player).col().unwrap();
        }

        let parsed: Result<usize, _> = trimmed.parse();
        match parsed {
            Err(e) => println!("Cannot parse number ({:?})", e.kind()),

            Ok(c) if c >= board.len() =>
                println!("Number entered must be less than {}", board.len()),
            Ok(c) if board[c].is_full() => println!("Column is already full"),
            Ok(c) => return c,
        }
    }
}

fn computer_move(board: &mut Board, bt: &mut Backtracker) -> usize {
    let started = Instant::now();
    let (nodes, score, col) = bt.backtrack_for(board, Piece::Computer).tuple();
    let elapsed = started.elapsed();

    let expected_outcome = score.expected_outcome();

    println!(
        "Computed move in {} (checked {} {}, {}, score {})",
        format_duration(elapsed, 2),
        nodes,
        if nodes == 1 { "node" } else { "nodes" },
        expected_outcome.to_str(Piece::Computer),
        score,
    );
    col.unwrap()
}
