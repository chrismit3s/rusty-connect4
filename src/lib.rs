#![feature(test)]
#![feature(associated_type_bounds)]
extern crate test;

use pyo3::prelude::*;

pub const SEED: u128 = 0xCAFE_BABE_1337_7000;

pub mod backtracker;
pub mod bitrepr;
pub mod game;
pub mod helper;

mod wrapper;

use wrapper::Wrapper;

#[pymodule]
fn connect4(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_class::<Wrapper>()?;
    Ok(())
}
