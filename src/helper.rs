use std::time::Duration;

#[inline]
pub fn offset(x: usize, offset: isize) -> usize {
    x.wrapping_add(offset as usize)
}

pub fn format_duration(d: Duration, n: usize) -> String {
    const UNITS: &[(&str, u128)] = &[
        ("ns", 1000),
        ("us", 1000),
        ("ms", 1000),
        ("s", 60),
        ("min", 60),
        ("h", u128::MAX),
    ];

    UNITS
        .iter()
        .scan(d.as_nanos(), |remaining, (unit, size)| {
            let value = *remaining % size;
            *remaining /= size;
            Some((value, unit))
        })
        .filter(|&(v, _)| v > 0)
        .collect::<Vec<_>>()
        .into_iter()
        .rev()
        .take(n)
        .map(|(v, u)| format!("{}{}", v, u))
        .reduce(|s, vu| s + " " + &vu)
        .unwrap_or_else(|| String::from("0ns"))
}

pub fn cartesian_product<A, B, I, J>(i: I, j: J) -> impl Iterator<Item = (A, B)>
where
    A: Clone,
    I: IntoIterator<Item = A> + Clone,
    J: IntoIterator<Item = B> + Clone,
{
    i
        .into_iter()
        .flat_map(move |a| j.clone().into_iter().map(move |b| (a.clone(), b)))
}
