use std::collections::HashMap;

use crate::backtracker::alphabeta::AlphaBeta;
use crate::backtracker::backtrackreturn::BacktrackReturn;
use crate::bitrepr::{self, BitRepr, FromBitRepr};
use crate::game::Board;

type Return = (AlphaBeta, Option<u8>, BacktrackReturn);
type Key = <Board as BitRepr>::Repr;
type Value = <Return as BitRepr>::Repr;

impl BitRepr for Return {
    type Repr = u64;

    const SIZE: usize = Self::Repr::BITS as usize;

    fn bit_repr(&self) -> Self::Repr {
        let (alphabeta, depth, btreturn) = *self;
        let btparams: Self::Repr = bitrepr::combine(alphabeta, depth);
        bitrepr::combine(btparams, btreturn)
    }
}

impl FromBitRepr for Return {
    fn from_bit_repr(repr: Self::Repr) -> Self {
        let (btparams, btreturn): (Self::Repr, BacktrackReturn) = bitrepr::extract(repr as u128); // TODO scuffed fix
        let (alphabeta, depth): (AlphaBeta, Option<u8>) = bitrepr::extract(btparams);
        (alphabeta, depth, btreturn)
    }
}

#[derive(Clone, PartialEq, Eq, Debug)]
pub struct Cache(HashMap<Key, Value>);

impl Cache {
    pub fn new() -> Self {
        Self(HashMap::with_capacity(200_000_000))
    }

    pub fn clear(&mut self) {
        self.0.clear();
    }

    pub fn get_key(&self, key: Key) -> Option<Return> {
        self.0.get(&key).copied().map(FromBitRepr::from_bit_repr)
    }

    pub fn get(&self, board: Board) -> Option<Return> {
        self.get_key(board.bit_repr())
    }

    pub fn set_key(&mut self, key: Key, value: Return) {
        self.0.insert(key, value.bit_repr());
    }

    pub fn set(&mut self, board: Board, value: Return) {
        self.set_key(board.bit_repr(), value);
    }
}

impl Default for Cache {
    fn default() -> Self {
        Self::new()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use crate::game::Board;
    use crate::game::Score;

    #[test]
    fn test_value_fixpoint() {
        for alpha in (Score::MIN_VALUE..=Score::MAX_VALUE).step_by(23) {
            for beta in (alpha..=Score::MAX_VALUE).rev().step_by(31) {
                for depth in (0..50).step_by(17) {
                    for score in (alpha..=beta).step_by(7) {
                        for c in 0..Board::DEFAULT_COLS {
                            let value: Return = (
                                AlphaBeta::from(Score::from(alpha), Score::from(beta)),
                                Some(depth),
                                BacktrackReturn::from(0, Score::from(score), Some(c)),
                            );
                            assert_eq!(value, Return::from_bit_repr(value.bit_repr()));
                        }
                    }
                }
            }
        }
    }
}
