use std::fmt::{self, Display, Formatter};

use crate::bitrepr::{self, BitRepr, FromBitRepr};
use crate::game::Piece;
use crate::game::Score;

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub struct AlphaBeta {
    alpha: Score,
    beta: Score,
}

impl AlphaBeta {
    pub fn full() -> Self {
        Self {
            alpha: Score::MIN,
            beta: Score::MAX,
        }
    }

    pub fn from(alpha: Score, beta: Score) -> Self {
        Self { alpha, beta }
    }

    pub fn update(&mut self, turn: Piece, score: Score) -> bool {
        if turn.is_maximizing() {
            self.alpha = self.alpha.max(score);
        } else {
            self.beta = self.beta.min(score);
        }
        self.alpha >= self.beta
    }

    pub fn clamp(&mut self, other: &Self) {
        self.alpha = self.alpha.clamp(other.alpha, other.beta);
        self.beta = self.beta.clamp(other.alpha, other.beta);
    }

    pub fn contains(&self, other: &Self) -> bool {
        self.alpha <= other.alpha && other.beta <= self.beta
    }
}

impl BitRepr for AlphaBeta {
    type Repr = u16;

    const SIZE: usize = 2 * Score::SIZE;

    fn bit_repr(&self) -> Self::Repr {
        bitrepr::combine(self.alpha, self.beta)
    }
}

impl FromBitRepr for AlphaBeta {
    fn from_bit_repr(repr: Self::Repr) -> Self {
        let (alpha, beta) = bitrepr::extract(repr);
        Self::from(alpha, beta)
    }
}

impl Display for AlphaBeta {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        f.write_str(if f.alternate() { "[alpha=" } else { "[" })?;
        f.pad_integral(self.alpha >= Score::ZERO, "", &self.alpha.abs().to_string())?;
        f.write_str(if f.alternate() { ", beta=" } else { " " })?;
        f.pad_integral(self.beta >= Score::ZERO, "", &self.beta.abs().to_string())?;
        f.write_str("]")?;
        Ok(())
    }
}
