use crate::backtracker::AlphaBeta;
use crate::backtracker::BacktrackReturn;
use crate::backtracker::Cache;
use crate::bitrepr::BitRepr;
use crate::game::Board;
use crate::game::Piece;

#[derive(Clone, PartialEq, Eq, Debug)]
pub struct Backtracker {
    cache: Cache,
    depth: Option<u8>,
}

impl Backtracker {
    pub fn with_depth(depth: u8) -> Self {
        Self::new(Some(depth))
    }

    pub fn unlimited() -> Self {
        Self::new(None)
    }

    pub fn new(depth: Option<u8>) -> Self {
        Self {
            cache: Cache::new(),
            depth,
        }
    }

    pub fn clear(&mut self) {
        self.cache.clear();
    }

    pub fn backtrack_for(&mut self, board: &mut Board, turn: Piece) -> BacktrackReturn {
        self.backtrack(board, turn, AlphaBeta::full(), self.depth)
    }

    //pub fn deepening_backtrack(
    //    &mut self,
    //    board: &mut Board,
    //    turn: Piece,
    //    mut alphabeta: AlphaBeta,
    //    depth: Option<u8>,
    //) -> (u32, Score, Option<usize>) {
    //    let mut total_nodes = 1;
    //    let mut best = turn.other().score();
    //    let mut backtrack_for = None;
    //    for d in 0..depth.unwrap_or(u8::MAX) {
    //        for c in self.moves(turn, board).into_iter() {
    //            let (nodes, score, _) = board.eval_with_mut(c, turn, |board| {
    //                self.backtrack(board, turn.other(), alphabeta, Some(d))
    //            });
    //            total_nodes += nodes;

    //            // update best values
    //            if backtrack_for.is_none() || turn.cmp(&score, &best).is_gt() {
    //                backtrack_for = Some(c);
    //                best = score;
    //            }

    //            // check for cutoff and update
    //            if alphabeta.update(turn, score) {
    //                break;
    //            }
    //        }
    //    }

    //    return (total_nodes, best, backtrack_for);
    //}

    pub fn backtrack(
        &mut self,
        board: &mut Board,
        turn: Piece,
        alphabeta: AlphaBeta,
        depth: Option<u8>,
    ) -> BacktrackReturn {
        let key = board.bit_repr();
        match self.cache.get_key(key) {
            Some((cached_alphabeta, cached_depth, ret))
                if cached_alphabeta.contains(&alphabeta)
                    && depth.unwrap_or(u8::MAX) <= cached_depth.unwrap_or(u8::MAX) =>
                ret,
            _ => {
                let ret = self._backtrack(board, turn, alphabeta, depth);
                self.cache.set_key(key, (alphabeta, depth, ret));
                ret
            },
        }
    }

    fn _backtrack(
        &mut self,
        board: &mut Board,
        turn: Piece,
        mut alphabeta: AlphaBeta,
        depth: Option<u8>,
    ) -> BacktrackReturn {
        if let Some(0) = depth {
            return BacktrackReturn::new(board.score());
        }

        if let Some(outcome) = board.outcome() {
            return BacktrackReturn::new(outcome.score());
        }

        let next_depth = depth.map(|d| d - 1);
        let other = turn.other();
        let mut best = BacktrackReturn::new(other.score());
        for c in self.moves(turn, board).into_iter() {
            let ret = board.eval_with_mut(c, turn, |board| {
                self.backtrack(board, other, alphabeta, next_depth)
            });

            best.update(turn, c, ret);
            if alphabeta.update(turn, best.score()) {
                break;
            }
        }

        best
    }

    fn moves(&self, turn: Piece, board: &mut Board) -> Vec<usize> {
        let mut moves: Vec<_> = board.iter_possible_moves().collect();
        //moves.sort_by_cached_key(|&c| board.eval_with(c, turn, |board| board.score()));
        moves.sort_by_cached_key(|&c| board.move_score(c, turn));
        moves
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use test::Bencher;

    use rand_pcg::Pcg64Mcg;

    use crate::SEED;

    fn bench_random_ncols(n: usize, b: &mut Bencher) {
        let mut rng = Pcg64Mcg::new(SEED);
        let mut board = Board::random(n, &mut rng);
        let mut bt = Backtracker::unlimited();
        b.iter(|| {
            bt.backtrack_for(&mut board, Piece::Computer);
            bt.clear()
        });
    }

    fn bench_empty_ncols(n: usize, b: &mut Bencher) {
        let mut board = Board::new(n);
        let mut bt = Backtracker::unlimited();
        b.iter(|| {
            bt.backtrack_for(&mut board, Piece::Computer);
            bt.clear()
        });
    }

    #[bench]
    fn bench_random_3cols(b: &mut Bencher) {
        bench_random_ncols(3, b);
    }

    #[bench]
    fn bench_random_4cols(b: &mut Bencher) {
        bench_random_ncols(4, b);
    }

    #[bench]
    fn bench_empty_3cols(b: &mut Bencher) {
        bench_empty_ncols(3, b);
    }

    #[bench]
    fn bench_empty_4cols(b: &mut Bencher) {
        bench_empty_ncols(4, b);
    }
}
