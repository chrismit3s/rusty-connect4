use std::cmp::PartialEq;

use crate::bitrepr::{self, BitRepr, FromBitRepr};
use crate::game::Piece;
use crate::game::Score;

#[derive(Copy, Clone, Debug, Eq)] // Impl eq to ignore nodes
pub struct BacktrackReturn {
    score: Score,
    nodes: u32,
    col: Option<usize>,
}

impl BacktrackReturn {
    pub fn from(nodes: u32, score: Score, col: Option<usize>) -> Self {
        Self { score, nodes, col }
    }

    pub fn new(score: Score) -> Self {
        Self {
            score,
            nodes: 1,
            col: None,
        }
    }

    pub fn nodes(&self) -> u32 {
        self.nodes
    }

    pub fn score(&self) -> Score {
        self.score
    }

    pub fn col(&self) -> Option<usize> {
        self.col
    }

    pub fn tuple(&self) -> (u32, Score, Option<usize>) {
        (self.nodes, self.score, self.col)
    }

    pub fn update(&mut self, turn: Piece, c: usize, ret: BacktrackReturn) {
        self.nodes += ret.nodes;

        if self.col.is_none() || turn.cmp(&ret.score, &self.score).is_gt() {
            self.col = Some(c);
            self.score = ret.score;
        }
    }
}

// this is the type we cast the .col attribute to, to save bits
type SmallCol = u8;

impl BitRepr for BacktrackReturn {
    type Repr = u16;

    const SIZE: usize = Self::Repr::BITS as usize;

    fn bit_repr(&self) -> Self::Repr {
        bitrepr::combine(self.score, self.col.map(|c| c as SmallCol))
    }
}

impl FromBitRepr for BacktrackReturn {
    fn from_bit_repr(repr: Self::Repr) -> Self {
        let (score, col): (Score, Option<SmallCol>) = bitrepr::extract(repr);
        Self::from(0, score, col.map(|c| c as usize))
    }
}

impl PartialEq<Self> for BacktrackReturn {
    fn eq(&self, other: &Self) -> bool {
        self.col == other.col && self.score == other.score
    }
}
