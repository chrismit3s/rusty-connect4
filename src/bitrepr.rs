use std::fmt::Debug;
use std::ops::{BitAnd, BitOr, Not, Shl, Shr};

pub fn combine<T, A, B>(a: A, b: B) -> T
where
    A: BitRepr<Repr: Into<T>>,
    B: BitRepr<Repr: Into<T>>,
    T: Shl<usize, Output = T> + BitOr<T, Output = T>,
{
    a.bit_repr().into() << B::SIZE | b.bit_repr().into()
}

pub fn extract<T, A, B>(t: T) -> (A, B)
where
    A: FromBitRepr + BitRepr,
    B: FromBitRepr + BitRepr,
    T: Copy
        + Default
        + TryInto<A::Repr, Error: Debug>
        + TryInto<B::Repr, Error: Debug>
        + Shl<usize, Output = T>
        + Shr<usize, Output = T>
        + BitAnd<T, Output = T>
        + Not<Output = T>,
{
    let a_mask = !(!T::default() << A::SIZE);
    let b_mask = !(!T::default() << B::SIZE);
    let a_bits = t >> B::SIZE;
    let b_bits = t;
    (
        A::from_bit_repr((a_bits & a_mask).try_into().unwrap()),
        B::from_bit_repr((b_bits & b_mask).try_into().unwrap()),
    )
}

pub trait BitRepr {
    type Repr: Copy + Shl + Shr + BitOr + BitAnd;

    const SIZE: usize;

    fn bit_repr(&self) -> Self::Repr;
}

pub trait FromBitRepr: BitRepr {
    fn from_bit_repr(repr: Self::Repr) -> Self;
}

macro_rules! int_bit_repr {
    ($t:ty, $r:ty) => {
        impl BitRepr for $t {
            type Repr = $r;

            const SIZE: usize = Self::Repr::BITS as usize;

            fn bit_repr(&self) -> Self::Repr {
                *self as Self::Repr
            }
        }

        impl FromBitRepr for $t {
            fn from_bit_repr(repr: Self::Repr) -> Self {
                repr as Self
            }
        }
    };
}

macro_rules! option_int_bit_repr {
    ($t:ty) => {
        impl BitRepr for Option<$t> {
            type Repr = <$t as BitRepr>::Repr;

            const SIZE: usize = Self::Repr::BITS as usize;

            fn bit_repr(&self) -> Self::Repr {
                self.map(|x| x + 1).unwrap_or(0) as Self::Repr
            }
        }

        impl FromBitRepr for Option<$t> {
            fn from_bit_repr(repr: Self::Repr) -> Self {
                repr.checked_sub(1)
            }
        }
    };
}

int_bit_repr!(u8, Self);
int_bit_repr!(u16, Self);
int_bit_repr!(u32, Self);
int_bit_repr!(u64, Self);
int_bit_repr!(u128, Self);
int_bit_repr!(usize, Self);

int_bit_repr!(i8, u8);
int_bit_repr!(i16, u16);
int_bit_repr!(i32, u32);
int_bit_repr!(i64, u64);
int_bit_repr!(i128, u128);
int_bit_repr!(isize, usize);

option_int_bit_repr!(u8);
option_int_bit_repr!(u16);
option_int_bit_repr!(u32);
option_int_bit_repr!(u64);
option_int_bit_repr!(u128);
option_int_bit_repr!(usize);
